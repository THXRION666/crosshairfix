#include <windows.h>

#define JMP '\xEB'
#define JP  '\x7A'
char* ptr = (char*)0x58E280;

void patch(char byte) {
    DWORD old_prot;
    VirtualProtect(ptr, 1, PAGE_READWRITE, &old_prot);
    *ptr = byte;
    VirtualProtect(ptr, 1, old_prot, &old_prot);
}

int __stdcall DllMain(HMODULE module, DWORD reason, LPVOID reserved) {
    switch (reason) {
        case DLL_PROCESS_ATTACH:
            patch(JMP);
            break;
        case DLL_PROCESS_DETACH:
            patch(JP);
            break;
    };
    return 1;
}